# Hello


Hi Dusan,

You will find 2 branches in this repository:

### Task 1 - The conversion task

Task 1, the conversion task is to be found in the ["conversion" branch](https://gitlab.com/alexandrufulop/dusan/tree/conversion)

```git clone -b conversion https://gitlab.com/alexandrufulop/dusan.git conversion```


### Task 2 - The PHP logger class

Task 2, the logger is in the ["logger" branch](https://gitlab.com/alexandrufulop/dusan/tree/logger)

```git clone -b logger https://gitlab.com/alexandrufulop/dusan.git logger```


Many thanks!
Alex